<?php

namespace App;

use App\User;
use App\Channel;
use Illuminate\Database\Eloquent\Model;
use App\Notifications\ReplyMarkedAsBestReply;

class Discussion extends Model
{
    protected $fillable = ['title', 'content', 'slug', 'channel_id', 'user_id', 'best_reply_id'];

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function replies()
    {
        return $this->hasMany(Reply::class);
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function bestReply()
    {
        return $this->belongsTo(Reply::class, 'best_reply_id');
    }

    public function markAsBestReply(Reply $reply)
    {
        $this->update([
            'best_reply_id' => $reply->id
        ]);

        if($reply->user->id === $this->author->id) {
            return;
        }

        $reply->user->notify(new ReplyMarkedAsBestReply($reply->discussion));
    }

    public function scopeFilterByChannels($builder)
    {
        if(request()->query('channel')) {
            $channel = Channel::where('slug', request()->query('channel'))->first();

            if($channel) {
                $builder->where('channel_id', $channel->id);
            }

            return $builder;
        }

        return $builder;
    }
}
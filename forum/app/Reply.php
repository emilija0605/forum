<?php

namespace App;

use App\User;
use App\Discussion;
use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    protected $fillable = ['reply', 'discussion_id', 'user_id'];

    public function discussion()
    {
        return $this->belongsTo(Discussion::class, 'discussion_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}

@extends('layouts.app')

@section('content')
<div class="card mb-5">
    @include('partials.discussion-header')

    <div class="card-body">
        <h5 class="text-center"><strong>{{ $discussion->title }}</strong></h5>

        <hr>

        {!! $discussion->content !!}

        @if($discussion->bestReply)
        <div class="card bg-success text-white mt-4 mb-2">
            <div class="card-header">
                <div class="d-flex justify-content-between align-items-center">
                    <div>
                        <img src="{{ Gravatar::src($discussion->bestReply->user->email)}}" width="30px"
                            style="border-radius: 50%" alt="">
                        <span class="ml-2"><strong>{{ $discussion->bestReply->user->name }}</strong></span>
                    </div>
                    <div>
                        <strong>Best reply</strong>
                    </div>
                </div>
            </div>
            <div class="card-body">
                {!! $discussion->bestReply->reply !!}
            </div>
        </div>
        @endif
    </div>
</div>

@foreach($discussion->replies()->paginate(3) as $reply)
<div class="card mb-2">
    <div class="card-header">
        <div class="d-flex justify-content-between align-items-center">
            <div>
                <img src="{{ Gravatar::src($reply->user->email) }}" width="30px" style="border-radius: 50%;" alt="">
                <span class="ml-2">{{ $reply->user->name }}</span>
            </div>

            @auth
            @if(auth()->user()->id === $discussion->user_id)
            <div>
                <form
                    action="{{ route('discussions.best-reply', ['discussion' => $discussion->slug, 'reply' => $reply->id]) }}"
                    method="POST">
                    @csrf
                    <button type="submit" class="btn btn-primary btn-sm">Mark as best reply</button>
                </form>
            </div>
            @endif
            @endauth
        </div>
    </div>
    <div class="card-body">
        {!! $reply->reply !!}
    </div>
</div>
@endforeach
<div class="mb-5">
    {{ $discussion->replies()->paginate(3)->links() }}
</div>

<div class="card">
    <div class="card-header">
        Add a reply
    </div>
    <div class="card-body">
        @auth

        @include('partials.errors')

        <form action="{{ route('replies.store', $discussion->slug) }}" method="POST">
            @csrf
            <input id="reply" type="hidden" name="reply">
            <trix-editor input="reply"></trix-editor>

            <button type="submit" class="btn btn-success btn-sm mt-2">Add reply</button>
        </form>

        @else

        <a href="{{ route('login') }}" class="btn btn-info">Sign in to add a reply</a>

        @endauth
    </div>
</div>
@endsection

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.1/trix.css">
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.1/trix.js"></script>
@endsection
@extends('layouts.app')

@section('content')
@if($discussions->count() > 0)
@foreach($discussions as $discussion)
<div class="card mb-4">
    @include('partials.discussion-header')

    <div class="card-body">
        <h5 class="text-center"><strong>{{ $discussion->title }}</strong></h5>
    </div>
</div>
@endforeach
@else
<h3 class="my-5">No discussions yet.</h3>
@endif

{{ $discussions->appends(['channel' => request()->query('channel')])->links() }}
@endsection
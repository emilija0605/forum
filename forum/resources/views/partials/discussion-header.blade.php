<div class="card-header">
    <div class="d-flex justify-content-between align-items-center">
        <div>
            <img src="{{ Gravatar::src($discussion->author->email) }}" width="40px" style="border-radius: 50%" alt="">
            <strong class="ml-2">{{ $discussion->author->name }}</strong class="ml-2">
        </div>
        <div>
            <a href="{{ route('discussions.show', $discussion->slug) }}" class="btn btn-success btn-sm">View</a>
        </div>
    </div>
</div>
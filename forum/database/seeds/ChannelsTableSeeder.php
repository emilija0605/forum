<?php

use App\Channel;
use Illuminate\Database\Seeder;

class ChannelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $channels = ['Laravel', 'Vue js', 'Angular', 'Node js'];

        foreach($channels as $channel) {
            Channel::create([
                'name' => $channel,
                'slug' => str_slug($channel)
            ]);
        }
    }
}
